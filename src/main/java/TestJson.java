
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

/**
 *read the value of the main.
 * @author seshie
 */
public class TestJson {

    public static void main(String[] args) throws IOException {
        var objectMapper = new ObjectMapper();
        var file = new File("src/main/resources/data/test.json");
        var jsonNode = objectMapper.readTree(file);
        System.out.println(jsonNode);
        System.out.println("Value: " + jsonNode.get("key").asText());
    }

}
