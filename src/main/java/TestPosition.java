
import com.fasterxml.jackson.databind.ObjectMapper;
import g54627.humbug.model.Position;
import java.io.IOException;

/**
 * read the value of the main.
 *
 * @author seshie
 */
public class TestPosition {

    public static void main(String[] args) throws IOException {
        var objectMapper = new ObjectMapper();
        var in = TestPosition.class.getResourceAsStream("/data/position.json");
        Position position = objectMapper.readValue(in, Position.class);
        System.out.println("Position read: " + position);
    }
}
