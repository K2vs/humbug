package g54627.humbug.controller;

import g54627.humbug.model.Direction;
import g54627.humbug.model.Level;
import g54627.humbug.model.LevelStatus;
import g54627.humbug.model.Model;
import g54627.humbug.model.Position;
import g54627.humbug.view.text.InterfaceView;

/**
 * The controller is responsible for the dynamics of, the game and the updating
 * of the view as you go.
 *
 * @author seshie
 */
public class Controller {

    private Model game;
    private InterfaceView view;

    /**
     * this constructor of the controler has two parameters the view and the
     * game.
     *
     * @param game the given game.
     * @param view the given view.
     */
    public Controller(Model game, InterfaceView view) {
        this.game = game;
        this.view = view;
    }

    /**
     * this method start the game and while the game is not over it keeps
     * displaying the board.
     *
     * @param nLevel according to the current level.
     */
    public void startGame(int nLevel) {
        while (game.getLevelStatus()
                != LevelStatus.NOT_STARTED) {
            game.startLevel(nLevel);
            while (game.getLevelStatus() == LevelStatus.IN_PROGRESS) {
                view.displayBoard(game.getBoard(), game.getAnimals());
                view.displayRemainingMoves(game.getRemainingMoves());
                Position position = view.askPosition();
                Direction direction = view.askDirection();
                try {
                    game.move(position, direction);

                } catch (IllegalArgumentException | NullPointerException e) {
                    view.displayError(e.getMessage());
                }
            }
            
            if (game.getLevelStatus() == LevelStatus.FAIL) {
                System.out.println("The game is Over, you can start again");
            }
            if (game.getLevelStatus() == LevelStatus.WIN) {
               // view.displayBoard(game.getBoard(), game.getAnimals());
                System.out.println("Well Done ★★★ ! moving to the next level ");
                nLevel++;
            }
        }
    }
}
