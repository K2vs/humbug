package g54627.humbug.main;

import g54627.humbug.controller.Controller;
import g54627.humbug.model.Game;
import g54627.humbug.view.text.View;
import java.util.Scanner;

/**
 * this is the main class where the game is played.
 *
 * @author seshie.
 *
 */
public class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(" ▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨| HUMBUG |▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨");
        Controller controller = new Controller(new Game(), new View());
        System.out.println("which level you want to start with ?");
        int level = keyboard.nextInt();
        controller.startGame(level);
        System.out.println("▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨▨");
    }
}
