package g54627.humbug.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 *
 * abstract class that represent what can animals do.
 *
 * @author seshie
 */
@JsonTypeInfo(use = Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
    @Type(value = Bumbelbee.class),
    @Type(value = Grasshopper.class),
    @Type(value = Ladybird.class),
    @Type(value = Snail.class),
    @Type(value = Spider.class),
    @Type(value = Butterfly.class),})

public abstract class Animal {
    
    private Position positionOnBoard;
    private boolean onStar;

    /**
     * the constructor of the position of the animal.
     *
     * @param positionOnBoard the current position on the board.
     */
    public Animal(Position positionOnBoard) {
        this.positionOnBoard = positionOnBoard;
        this.onStar = false;
    }

    /**
     * the empty constructor of Animal.
     */
    public Animal() {
    }

    /**
     * Get the value of onStar
     *
     * @return the value of onStar
     */
    public boolean isOnStar() {
        return onStar;
    }

    /**
     * Get the value of positionOnBoard
     *
     * @return the value of positionOnBoard
     */
    public Position getPositionOnBoard() {
        return positionOnBoard;
    }

    /**
     * setter of on star.
     *
     * @param onStar true if a animal is on star otherwise false.
     */
    public void setOnStar(boolean onStar) {
        this.onStar = onStar;
    }

    /**
     * set the position on board.
     *
     * @param positionOnBoard the current position.
     */
    public void setPositionOnBoard(Position positionOnBoard) {
        this.positionOnBoard = positionOnBoard;
    }

    /**
     * Animals can move around on the game board.Each animal has its own way of
     * moving
     *
     * @param board the given board
     * @param direction the given direction
     * @param animals the current animal
     * @return the next position of the animal.
     *
     */
    public abstract Position move(Board board,
            Direction direction, Animal... animals);

    /**
     * Browse the Animal board , and checks if the given position is occupied by
     * an another animals.
     *
     * @param position the given position
     * @param animal represent the current animal.
     * @return true if the next square is free , otherwise return false
     */
    protected boolean isValidPosition(Position position,
            Animal... animal) {
        boolean free = true;
        int index = 0;
        while (index < animal.length && free) {
            if (animal[index].getPositionOnBoard().equals(position)
                    && !animal[index].isOnStar()) {
                free = false;
            }
            index++;
        }
        return free;
    }

    /**
     * as long as the nextAnimalPos is not free , jump or flight to the next,
     * position.
     *
     * @param animalPos the initial position of the current animal.
     * @param nextAnimalPos the next position of the current animal.
     * @param direction the given direction.
     * @param board the given board.
     * @param animals the given animal.
     * @return the next position of the animal if it's possible.
     */
    protected Position tillFreeSquare(Position animalPos, Position nextAnimalPos,
            Direction direction, Board board, Animal... animals) {
        while (!isValidPosition(nextAnimalPos, animals)) {
            nextAnimalPos = nextAnimalPos.next(direction);
        }

        if (!board.isInside(nextAnimalPos)) {
            this.setPositionOnBoard(null);
            return null;
        }
        
        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos)
                == SquareType.STAR && isValidPosition(nextAnimalPos, animals)) {
            this.setOnStar(true);
            this.setPositionOnBoard(nextAnimalPos);
            board.removeStar(nextAnimalPos);
            return nextAnimalPos;
        }
        return nextAnimalPos;
    }
}
