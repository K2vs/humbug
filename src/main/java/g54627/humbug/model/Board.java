package g54627.humbug.model;

/**
 * Board is where the main game is played he has many squares when a square has
 * as value null it doesn't exist.
 *
 * @author seshie
 */
public class Board {

    /**
     * initialize the first level of the game when a square of the board is null
     * it doesn't represent a square.
     *
     * @return the initial board of the first level
     */
    public static Board getInitialBoard() {
        Square[][] boardInit = {
            {new Square(SquareType.GRASS), new Square(SquareType.GRASS), null},
            {null, new Square(SquareType.GRASS), new Square(SquareType.GRASS)},
            {null, null, new Square(SquareType.STAR)},};

        return new Board(boardInit);
    }

    private Square[][] squares;

    /**
     * the constructor of the board.
     *
     * @param squares the given Square tab.
     */
    public Board(Square[][] squares) {
        this.squares = squares;
    }

    /**
     * the empty contructor of board.
     */
    public Board() {
    }

    /**
     * given number of row on the board.
     *
     * @return the number of row in the given array.
     */
    public int getNbRow() {
        return squares.length;
    }

    /**
     * given number of column on the board.
     *
     * @return the number of column in the given array
     */
    public int getNbColumn() {
        return squares[0].length;
    }

    /**
     * Checks if the selected position is a valid square in the board.
     *
     * @param pos the given position to check
     * @throws IllegalArgumentException if the position is null.
     * @return true if square at pos is of SquareType Grass or Star, false
     *
     */
    public boolean isInside(Position pos) {
        boolean inside;
        if (pos == null) {
            throw new IllegalArgumentException("a position can't be null");
        }
        if (pos.getColumn() < 0
                || pos.getRow() < 0
                || pos.getRow() >= this.getNbRow()
                || pos.getColumn() >= this.getNbColumn()) {
            inside = false;

        } else if (squares[pos.getRow()][pos.getColumn()] == null) {
            inside = false;

        } else {
            inside = true;
        }
        return inside;
    }

    /**
     * returning the type of a square on the board.
     *
     * @param pos the given position
     * @throws IllegalArgumentException if the position is null.
     * @return type of the square grass or star.
     */
    public SquareType getSquareType(Position pos) {
        if (squares[pos.getRow()][pos.getColumn()] == null) {
            throw new IllegalArgumentException("positon defines no squares");
        }
        return squares[pos.getRow()][pos.getColumn()].getType();
    }

    /**
     * removes the star and change it into a grass when a animals arrived on it.
     *
     * @param pos the given position allowed by my teacher.(MCD).
     */
    public void removeStar(Position pos) {
        squares[pos.getRow()][pos.getColumn()].setType(SquareType.GRASS);

    }

    /**
     * Get the square
     *
     * @return square
     */
    public Square[][] getSquares() {
        return squares;
    }

    /**
     * Get the type of the square according to the given position.
     *
     * @param position the given position.
     * @return a position of a square.
     */
    public Square getSquare(Position position) {
        if (position == null) {
            throw new IllegalArgumentException("position can't be null");
        }
        if (!isInside(position)) {
            throw new IllegalArgumentException("not inside");
        }
        return this.squares[position.getRow()][position.getColumn()];
    }
}
