package g54627.humbug.model;

/**
 * represents a BumbelBee , The bumbelBee is an animal of the game that flight
 * over two squares even over an empty squares as long as the arrival square is
 * occupied he flights to the next one.
 *
 * @author seshie
 */
public class Bumbelbee extends Animal {

    /**
     * the constructor of BumbelBee.
     *
     * @param positionOnBoard the current position on Board.
     */
    public Bumbelbee(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of BumbelBee.
     */
    public Bumbelbee() {

    }

    /**
     * flights two squares in the indicated direction.
     *
     * @param board the given board.
     * @param direction the given direction.
     * @param animals represents the current animal(BumbelBee).
     * @return null if the animals falls, or the next direction of the animals.
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position animalPos = getPositionOnBoard();
        Position nextAnimalPos = animalPos.next(direction).next(direction);
        Position finalPos = tillFreeSquare(animalPos,
                nextAnimalPos, direction, board, animals);
        
        this.setPositionOnBoard(finalPos);
        return finalPos;
    }
}
