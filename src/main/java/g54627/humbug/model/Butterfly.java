package g54627.humbug.model;

/**
 * represents a Butterfly , The butterfly is an animal of the game that flight
 * over three squares even over an empty squares as long as the arrival square
 * is occupied he flights to the next one.
 *
 * @author seshie
 */
public class Butterfly extends Animal {

    /**
     * the constructor of Butterfly.
     *
     * @param positionOnBoard the current position on Board.
     */
    public Butterfly(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of Butterfly.
     */
    public Butterfly() {
    }

    /**
     * flights three squares in the indicated direction.
     *
     *
     * @param board the given board.
     * @param direction the given direction.
     * @param animals the current animal(Butterfly).
     * @return null if the animal fall, or the next position of the animal.
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position animalPos = getPositionOnBoard();
        Position nextAnimalPos = animalPos.next(direction)
                .next(direction).next(direction);
        Position finalPos = tillFreeSquare(animalPos,
                nextAnimalPos, direction, board, animals);

        this.setPositionOnBoard(finalPos);
        return finalPos;
    }
}
