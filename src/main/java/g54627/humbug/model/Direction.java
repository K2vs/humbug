package g54627.humbug.model;

/**
 * Direction to indicate which direction you want to move to represented by the
 * Cardinal points
 *
 * @author seshie
 */
public enum Direction {
    EAST(0, 1),
    WEST(0, -1),
    NORTH(-1, 0),
    SOUTH(1, 0),;
    private int deltaRow, deltaColumn;

    /**
     * indicate in which direction you want to move forward.
     *
     * @param deltaRow the given row
     * @param deltaColumn the given column
     */
    private Direction(int deltaRow, int deltaColumn) {

        this.deltaRow = deltaRow;
        this.deltaColumn = deltaColumn;
    }

    /**
     * Get the value of deltaRow
     *
     * @return the value of deltaRow
     */
    public int getDeltaRow() {
        return deltaRow;
    }

    /**
     * Get the value of deltaColumn
     *
     * @return the value of deltaColumn
     */
    public int getDeltaColumn() {
        return deltaColumn;
    }

    /**
     * gives the opposition direction.
     *
     * @return the opposite direction or by default null.
     */
    public Direction opposite() {
        switch (this) {
            case NORTH:
                return Direction.SOUTH;
            case SOUTH:
                return Direction.NORTH;
            case WEST:
                return Direction.EAST;
            case EAST:
                return Direction.WEST;
        }
        return null;// this value won't be given.
    }
}
