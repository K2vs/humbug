package g54627.humbug.model;

/**
 * The Game class has all elements needed to present a facade of the view. the
 * view only interact with this class.
 *
 * @author seshie
 */
public class Game implements Model {

    private Board board;
    private Animal[] animals;
    private int remainingMoves, currentLevel;
    private LevelStatus LevelStatus;

    /**
     * the constuctor of Game.
     */
    public Game() {
    }

    /**
     * Get the value of board
     *
     * @return the value of board
     *
     */
    @Override
    public Board getBoard() {
        return board;
    }

    /**
     * Get the value of animals
     *
     * @return the value of animals
     */
    @Override
    public Animal[] getAnimals() {
        return animals;
    }

    /**
     * initializes the game board, animals and remaniningMoves depending on
     * which level you are.
     *
     *
     * @param level the given integer that represents a level
     */
    @Override
    public void startLevel(int level) {
        LevelStatus = LevelStatus.NOT_STARTED;
        this.animals = Level.getLevel(level).getAnimals();
        this.board = Level.getLevel(level).getBoard();
        this.remainingMoves = Level.getLevel(level).getnMoves();
        LevelStatus = LevelStatus.IN_PROGRESS;
    }

    /**
     * moves if it's allowed the animals.
     *
     * @param position the current position.
     * @param direction the given direction.
     * @throws IllegalArgumentException if the position or direction are null.
     */
    @Override
    public void move(Position position, Direction direction) {
        if (getLevelStatus() == LevelStatus.NOT_STARTED) {
            throw new IllegalStateException("Level not started");
        }

        if (position == null || direction == null) {
            throw new IllegalArgumentException("a position and"
                    + "a direction can't be null");
        }

        int index = 0;
        while (index < getAnimals().length) {
            
            if (getAnimals()[index].getPositionOnBoard().equals(position)) {
                getAnimals()[index].move(getBoard(), direction, getAnimals());
                updateLevelStatus();
            }
            index++;
        }
    }
 
    /**
     * represent the number of movement a players has for a specific level.
     *
     * @return the number of movements left.
     */
    @Override
    public int getRemainingMoves() {
        return remainingMoves;
    }

    /**
     * checks the status of the level , a level has 4 stages possible win ,
     * fail,not stated or in Progress.
     */
    private void updateLevelStatus() {
        boolean allAnimalsOnStar = false;
        LevelStatus = LevelStatus.IN_PROGRESS;
        remainingMoves--;
        for (Animal animal : animals) {
            if (!animal.isOnStar()) {
                allAnimalsOnStar = true;
            }
            
            if (animal.getPositionOnBoard() == null
                    || (remainingMoves <= 0 && !animal.isOnStar())) {
                LevelStatus = LevelStatus.FAIL;
            }
        }
            if (!allAnimalsOnStar) {
                LevelStatus = LevelStatus.WIN;
            }
        }

    /**
     * specifies if the status of the level , a level is completed when all the
     * animals are on a Star.
     *
     * a status can have four different states.
     *
     *
     * @return the current status of the level.
     */
    @Override
    public LevelStatus getLevelStatus() {
        return LevelStatus;
    }
}
