package g54627.humbug.model;

/**
 * represents a Grasshopper , The Grasshopper is an animal of the game that
 * jumps one square in the direction indicated as long as the arrival square is
 * occupied he jump to the next one.
 *
 *
 * @author seshie
 */
public class Grasshopper extends Animal {

    /**
     * the constructor of Grasshopper.
     *
     * @param positionOnBoard the current position on Board.
     */
    public Grasshopper(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of Grasshopper.
     */
    public Grasshopper() {
    }

    /**
     * moves one square in the direction indicated , but as long as there is a
     * wall or animal he jump over it.
     *
     * @param board the given board
     * @param direction the given direction
     * @param animals represent the current animal (grassShopper).
     * @return return newSnailpos,snailPos if it still on the board , otherwise
     * null.
     *
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position animalPos = getPositionOnBoard();
        Position nextAnimalPos = animalPos.next(direction);
        Position finalPos = tillFreeSquare(animalPos, nextAnimalPos,
                direction, board, animals);

        this.setPositionOnBoard(finalPos);
        return finalPos;
    }

}
