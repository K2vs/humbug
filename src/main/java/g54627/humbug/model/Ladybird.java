package g54627.humbug.model;

/**
 * represents a Ladybird , The Ladybird is an animal of the game that moves two
 * squares in the direction indicated on condition that the space is not
 * occupied by another animals , ortherwise he moves only one square or stay at
 * the same position.
 *
 * @author seshie
 */
public class Ladybird extends Animal {

    /**
     * the constructor of LadyBird.
     *
     * @param positionOnBoard the current position on board.
     */
    public Ladybird(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of Ladybird.
     */
    public Ladybird() {
    }

    /**
     * moves if it's allowed.
     *
     * @param board the given board.
     * @param direction the given board.
     * @param animals the current animal(Ladybird).
     * @return null if the animals falls , the next direction if he was stopped
     * by an another animals or wall or return the final position.
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position initAnimalPos = getPositionOnBoard();
        Position nextAnimalPos = initAnimalPos.next(direction);
        Position finalAnimalPos = nextAnimalPos.next(direction);

        if (!board.isInside(nextAnimalPos)
                || !board.isInside(finalAnimalPos)) {
            if (board.getSquare(initAnimalPos).hasWall(direction)) {
                return initAnimalPos;
            } else {
                this.setPositionOnBoard(null);
                return null;
            }
        }

        if (!isValidPosition(nextAnimalPos, animals)) {
            this.setPositionOnBoard(initAnimalPos);
            return initAnimalPos;
        }

        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos)
                == SquareType.STAR
                && !isValidPosition(finalAnimalPos, animals)) {
            this.setOnStar(true);
            board.removeStar(nextAnimalPos);
            return nextAnimalPos;

        } else if (!isValidPosition(finalAnimalPos, animals)) {
            this.setPositionOnBoard(nextAnimalPos);
            return nextAnimalPos;
        }

        if (board.getSquare(initAnimalPos).hasWall(direction)
                || board.getSquare(nextAnimalPos)
                        .hasWall(direction.opposite())) {
            this.setPositionOnBoard(initAnimalPos);
            return initAnimalPos;

        } else if (board.getSquare(nextAnimalPos).hasWall(direction)
                || board.getSquare(finalAnimalPos)
                        .hasWall(direction.opposite())) {
            this.setPositionOnBoard(nextAnimalPos);
            return nextAnimalPos;
        }

        if (board.isInside(finalAnimalPos)
                && board.getSquareType(finalAnimalPos)
                == SquareType.STAR
                && isValidPosition(finalAnimalPos, animals)) {
            this.setOnStar(true);
            this.setPositionOnBoard(nextAnimalPos);
            board.removeStar(finalAnimalPos);
            return finalAnimalPos;
        }
        this.setPositionOnBoard(finalAnimalPos);
        return finalAnimalPos;
    }
}
