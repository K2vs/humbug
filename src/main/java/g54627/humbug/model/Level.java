package g54627.humbug.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

/**
 *
 * @author seshie
 */
public class Level {

    /**
     * get the level that you want to play.
     *
     * @param level the level of the list of levels.
     * @return a level of the game.
     */
    public static Level getLevel(int level) {
        return readLevel(level);
    }

    /**
     * read the levels.
     *
     * @param n an integer that represent a number of level.
     * @return the level that you want to play.
     */
    private static Level readLevel(int n) {
        try {
            var objectMapper = new ObjectMapper();
            var inputStream = Level.class.getResourceAsStream(
                    "/data/level-" + n + ".json");
            var level = objectMapper.readValue(inputStream, Level.class);
            return level;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    private Board board;
    private Animal[] animals;
    private int nMoves;

    /**
     * the constructor of board.
     *
     * @param board
     * @param animals
     * @param nMoves
     */
    public Level(Board board, Animal[] animals, int nMoves) {
        this.board = board;
        this.animals = animals;
        this.nMoves = nMoves;
    }

    /**
     * the empty constructor of Level.
     */
    public Level() {
    }

    /**
     * get the board.
     *
     * @return the board of the level.
     */
    public Board getBoard() {
        return board;
    }

    /**
     * get the animals.
     *
     * @return the animals of the level.
     */
    public Animal[] getAnimals() {
        return animals;
    }

    /**
     * get the nMoves allowed for the level.
     *
     * @return the number of moves left.
     */
    public int getnMoves() {
        return nMoves;
    }

}
