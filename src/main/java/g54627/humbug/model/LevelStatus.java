package g54627.humbug.model;

/**
 * represent different states of the game.
 *
 * @author seshie
 */
public enum LevelStatus {
    NOT_STARTED,
    IN_PROGRESS,
    FAIL,
    WIN;
}
