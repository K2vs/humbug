package g54627.humbug.model;

/**
 *
 * the model will contain for us the classes that define the elements as well as
 * the main logic of the application. .
 *
 * @author seshie
 */
public interface Model {

    /**
     * get the number of mouvement left.
     *
     * @return the number of mouvement left.
     */
    int getRemainingMoves();

    /**
     * Get the value of board
     *
     * @return the value of board
     *
     */
    Board getBoard();

    /**
     * Get the value of animals
     *
     * @return the value of animals
     *
     */
    Animal[] getAnimals();

    /**
     * initializes the games board and the animals for the level.
     *
     *
     * @param level the given integer that represents a level
     *
     */
    void startLevel(int level);

    /**
     * specifies if the status of the level , a level is completed when all the
     * animals are on a Star.
     *
     *
     * @return the current status of the level.
     */
    LevelStatus getLevelStatus();

    /**
     * moves if it's allowed the animal.
     *
     * @param position the current position.
     * @param direction the given direction.
     */
    void move(Position position, Direction direction);
}
