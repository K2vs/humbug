package g54627.humbug.model;

/**
 * Position is the position on the board. A position is a number of row and
 * column that form a position.
 *
 * @author seshie
 */
public class Position {

    private final int row, column;

    /**
     * the constructor of Position. helps for json.
     */
    public Position() {
        row = 1;
        column = 0;
    }

    /**
     * find your way on the game board
     *
     * @param row the given number row to set position.
     * @param column the given number column to set position.
     */
    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Get the value of row
     *
     * @return the value of row
     */
    public int getRow() {
        return row;
    }

    /**
     * Get the value of column
     *
     * @return the value of column
     */
    public int getColumn() {
        return column;
    }

    /**
     * tell if two objet have the same HashCode.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.row;
        hash = 11 * hash + this.column;
        return hash;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the given object
     * @return true if the are the same , otherwise false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.row != other.row) {
            return false;
        }
        return this.column == other.column;
    }

    /**
     * returns the string representation of the object.
     *
     * @return the position such as [X,X]
     */
    @Override
    public String toString() {
        return "Position : [" + row + "," + column + ']';
    }

    /**
     * returns the position next to the given direction
     *
     * @param d the given direction
     * @return the new position to move forward.
     */
    public Position next(Direction d) {
        Position newPosition = new Position(this.getRow() + d.getDeltaRow(),
                this.getColumn() + d.getDeltaColumn());
        return newPosition;
    }
}
