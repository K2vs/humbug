package g54627.humbug.model;

/**
 * represents a snail , The snail is an animal of the game that moves one square
 * in the direction indicated on condition that the space is not occupied by
 * another animals.
 *
 * @author seshie
 */
public class Snail extends Animal {

    /**
     * the constructor of Snail.
     *
     * @param positionOnBoard the current position .
     */
    public Snail(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of Snail.
     */
    public Snail() {
    }

    /**
     * moves one square in the direction indicated.
     *
     * @param board the given board
     * @param direction the given direction
     * @param animals represent the current animal (Snail).
     * @return return newSnailpos,snailPos if it still on the board , otherwise
     * null.
     *
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position animalPos = getPositionOnBoard();
        Position nextAnimalPos = animalPos.next(direction);

        if (!board.isInside(nextAnimalPos)) {
            this.setPositionOnBoard(null);
            return null;
        }
        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos) == SquareType.GRASS
                && board.getSquare(animalPos).hasWall(direction)) {
            this.setPositionOnBoard(animalPos);
            return animalPos;
        }
        if (board.isInside(nextAnimalPos)
                && board.getSquareType(animalPos) == SquareType.GRASS
                && board.getSquare(nextAnimalPos)
                        .hasWall(direction.opposite())) {
            this.setPositionOnBoard(animalPos);
            return animalPos;
        }
        if (!isValidPosition(nextAnimalPos, animals)
                && board.isInside(animalPos) && board.getSquareType(animalPos)
                == SquareType.GRASS) {
            this.setPositionOnBoard(animalPos);
            return animalPos;
        }

        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos)
                == SquareType.GRASS
                && isValidPosition(nextAnimalPos, animals)) {
            this.setPositionOnBoard(nextAnimalPos);
            return nextAnimalPos;
        }

        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos)
                == SquareType.STAR
                && isValidPosition(nextAnimalPos, animals)) {
            this.setOnStar(true);
           this.setPositionOnBoard(nextAnimalPos);
            board.removeStar(nextAnimalPos);
            return nextAnimalPos;

        }
        return nextAnimalPos;
    }
}
