package g54627.humbug.model;

/**
 * represents a spider. The spider is an animal of the game that moves in the
 * direction indicated as long as it does not go through an obstacle , or meet
 * another animal.
 *
 * @author seshie
 */
public class Spider extends Animal {

    /**
     * the constructor of spider. an animals knows his position on the board.
     *
     * @param positionOnBoard the current position on board of the animal.
     */
    public Spider(Position positionOnBoard) {
        super(positionOnBoard);
    }

    /**
     * the empty constructor of Spider.
     */
    public Spider() {
    }

    /**
     * depending on the direction , browse the board and see if there is a wall
     * or any animals on the direction of the current animal to stop him.
     *
     * @param board the given board.
     * @param position the current position of the spider..
     * @param direction the given direction.
     * @param animal the animal of the game.
     * @return the position of the spider.
     *
     * Allowed by my teacher.(MCD).
     */
    private Position freeWay(Board board, Position position,
            Direction direction, Animal... animal) {
        if (direction == Direction.EAST || direction == Direction.WEST) {
            for (int i = 0; i < board.getNbColumn(); i++) {
                position = position.next(direction);
                if (board.getSquare(getPositionOnBoard()).hasWall(direction)) {
                    return getPositionOnBoard();
                    // case the wall is directly there.
                }
                if (board.isInside(position)
                        && board.getSquare(position)
                                .hasWall(direction.opposite())) {
                    return position.next(direction.opposite());
                    // case the wall is directly opposite at the next square.
                }
                if (board.isInside(position)
                        && board.getSquare(position).hasWall(direction)) {
                    return position;
                }
                int index = 0;
                while (index < animal.length) {
                    if (position.equals(animal[index].getPositionOnBoard())) {
                        return new Position(position.getRow(),
                                position.getColumn() - 1);
                    }
                    index++;
                }
            }
        } else if (direction == Direction.NORTH || direction == Direction.SOUTH) {
            for (int i = 0; i < board.getNbRow(); i++) {
                position = position.next(direction);
                int index = 0;
                if (board.getSquare(getPositionOnBoard()).hasWall(direction)) {
                    return getPositionOnBoard();
                }
                if (board.isInside(position)
                        && board.getSquare(position).
                                hasWall(direction.opposite())) {
                    return position.next(direction.opposite());
                }
                if (board.isInside(position)
                        && board.getSquare(position).hasWall(direction)) {
                    return position;
                }

                while (index < animal.length) {
                    if (position.equals(animal[index].getPositionOnBoard())) {
                        return new Position(position.getRow() - 1,
                                position.getColumn());
                    }
                    index++;
                }
            }
        }
        return position;
    }

    /**
     * moves in the given direction until he meets an obstacle.
     *
     * @param board the given board.
     * @param direction the given direction.
     * @param animals the current animal.
     * @return null if the animals falls, or return the current or next position
     * of the animals.
     */
    @Override
    public Position move(Board board, Direction direction, Animal... animals) {
        Position animalPos = getPositionOnBoard();
        Position nextAnimalPos = freeWay(board, animalPos, direction, animals);

        if (!board.isInside(nextAnimalPos)) {
            this.setPositionOnBoard(null);
            return null;
        }
        if (board.isInside(nextAnimalPos)
                && board.getSquareType(nextAnimalPos) == SquareType.STAR) {
            this.setOnStar(true);
            this.setPositionOnBoard(nextAnimalPos);
            board.removeStar(nextAnimalPos);
            return nextAnimalPos;
        }
        this.setPositionOnBoard(nextAnimalPos);
        return nextAnimalPos;
    }
}
