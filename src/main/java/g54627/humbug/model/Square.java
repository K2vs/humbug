package g54627.humbug.model;

/**
 * Square on the board. A square has a type grass or star and it’s all. square
 * does not know where it is on the board.
 *
 * @author seshie
 */
public class Square {

    private SquareType type;
    private boolean northWall, southWall, westWall, eastWall;

    /**
     * Constructor of Square on board.∗ @param type Square is grass or star
     *
     * @param type the given type.
     */
    public Square(SquareType type) {
        this.type = type;
        northWall = false;
        southWall = false;
        westWall = false;
        eastWall = false;
    }

    /**
     *the empty constructor of Square.
     */
    public Square() {
    }

    /**
     * Set the value of northWall
     *
     * @param northWall new value of northWall
     */
    public void setNorthWall(boolean northWall) {
        this.northWall = northWall;
    }

    /**
     * Set the value of southWall.
     *
     * @param southWall new value of northWall
     */
    public void setSouthWall(boolean southWall) {
        this.southWall = southWall;
    }

    /**
     * Set the value of westWall
     *
     * @param westWall new value of northWall
     */
    public void setWestWall(boolean westWall) {
        this.westWall = westWall;
    }

    /**
     * Set the value of eastWall
     *
     * @param eastWall new value of northWall
     */
    public void setEastWall(boolean eastWall) {
        this.eastWall = eastWall;
    }

    /**
     * ∗ Simple getter of type. ∗
     *
     * @return type of Square
     */
    public SquareType getType() {
        return type;
    }

    /**
     * set the type
     *
     * @param type the given type.
     */
    public void setType(SquareType type) {
        this.type = type;
    }

    /**
     *
     * @param direction the given direction.
     * @return in which direction the wall is.
     */
    public boolean hasWall(Direction direction) {
        switch (direction.name()) {
            case "NORTH":
                return northWall;
            case "SOUTH":
                return southWall;
            case "WEST":
                return westWall;
            case "EAST":
                return eastWall;
        }
        return false;// wont return this value.
    }
}
