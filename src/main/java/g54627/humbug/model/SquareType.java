package g54627.humbug.model;

/**
 * SquareType represents the type of a square on the board. Square are grass or
 * star when there represent arrival.
 *
 * @author g54627
 */
public enum SquareType {
    GRASS,
    STAR,;
}
