package g54627.humbug.view.text;

import g54627.humbug.model.Animal;
import g54627.humbug.model.Board;
import g54627.humbug.model.Direction;
import g54627.humbug.model.Position;

/**
 * represents the interface of the view.
 *
 * @author seshie
 */
public interface InterfaceView {

    /**
     * displaying the game board
     *
     * @param board the given game board of the first level.
     * @param animals the current animals.
     */
    void displayBoard(Board board, Animal... animals);
    
    /**
     * asking the user to encode a position.
     *
     * @return the position
     */
    public Position askPosition();

    /**
     * return an error message
     *
     * @param message the given message
     */
    public void displayError(String message);

    /**
     * asking the user to encode a direction.
     *
     * @return the direction.
     */
    public Direction askDirection();

    /**
     * display the number of mouvements left.
     *
     * @param remainingMoves an integer that represent the number of movement
     * left.
     *
     */
    void displayRemainingMoves(int remainingMoves);
}
