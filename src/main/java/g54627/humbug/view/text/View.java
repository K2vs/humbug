package g54627.humbug.view.text;

import g54627.humbug.model.Animal;
import g54627.humbug.model.Board;
import g54627.humbug.model.Bumbelbee;
import g54627.humbug.model.Butterfly;
import g54627.humbug.model.Direction;
import g54627.humbug.model.Grasshopper;
import g54627.humbug.model.Ladybird;
import g54627.humbug.model.Position;
import g54627.humbug.model.Snail;
import g54627.humbug.model.Spider;
import g54627.humbug.model.SquareType;
import java.util.Scanner;

/**
 * this class represents the view of the game. implements InterfaceView.
 *
 * @author seshie
 */
public class View implements InterfaceView {
    static final String RED = "\033[41m";
    static final String YELLOW = "\033[33m";
    static final String GREEN_BG= "\033[42m";
    static final String DEFAULT="\033[0m";
    
    /*
      * Robust reading of an integer.
      *
      * As long as the user input is not an integer the method requests
      * a new entry.
      *
      * @param message message to display.
      * @return the whole entered by the user.
     */
    private int readInteger(String message) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(message);
        while (!keyboard.hasNextInt()) {
            keyboard.next();
            System.out.println("you didn't enter a integer ");
            System.out.println(message);
        }
        return keyboard.nextInt();
    }

    /**
     * displaying the game board
     *
     * @param board the given game board.
     * @param animals the animals on the board according to the level.
     */
    @Override
    public void displayBoard(Board board, Animal... animals) {
  String[][] gameArea = new String[board.getNbRow() * 5][board.getNbColumn()*5];
        for ( int lg = 0 ; lg < gameArea.length;  lg++) {
            for ( int col = 0; col < gameArea[lg].length; col++) {
                Position pos = new Position(lg,col);
                if (board.isInside(pos)) {

             traceMur(lg*5, col*5, gameArea);
             traceMur(lg*5+4, col*5, gameArea);
                    
                gameArea[lg*5+1][col*5]=
                gameArea[lg*5+2][col*5]=
                gameArea[lg*5+3][col*5]=
                            GREEN_BG+" ▮"+DEFAULT;
                    
                gameArea[lg*5+1][col*5+4]=
                gameArea[lg*5+2][col*5+4]=
                gameArea[lg*5+3][col*5+4]=
                           GREEN_BG+" ▮"+DEFAULT;
                    
                gameArea[lg*5+1][col*5+1]=
                gameArea[lg*5+1][col*5+3]=
                gameArea[lg*5+3][col*5+1]=
                gameArea[lg*5+3][col*5+3]=
                   GREEN_BG+ "  "+DEFAULT;
                    
               gameArea[lg*5+1][col*5+2]=
               gameArea[lg*5+3][col*5+2]=
               gameArea[lg*5+2][col*5+3]=
               gameArea[lg*5+2][col*5+1]=
                GREEN_BG+ "  "+DEFAULT;
   
       if (board.getSquareType(pos)==SquareType.STAR) {
                gameArea[lg*5+2][col*5+2]=GREEN_BG+YELLOW+" ★"+DEFAULT;
       } 
       
       if (board.getSquareType(pos)==SquareType.GRASS) {
                gameArea[lg*5+2][col*5+2]=GREEN_BG+"  " +DEFAULT;
          }
       
    }          
     }
     }
        
        displayAnimals(gameArea, animals);
        displayWalls(gameArea, board);
        displayArray(gameArea);
    }
    
    /**
     * browse the Board and print all walls according to the level.
     * @param gameArea the given String tab.
     * @param board the curent board game.
     * @param animals the animals on the game , according to the level. 
     */
    private void displayWalls(String [][] gameArea ,
            Board board ){
  for (int lg = 0 ; lg < gameArea.length;  lg++) {
     for (int col = 0; col < gameArea[lg].length; col++) {
          Position pos = new Position(lg,col);
 if (board.isInside(pos)) {
                 if (board.getSquare(pos).hasWall(Direction.NORTH)) {
                gameArea[lg*5+1][col*5+2]=GREEN_BG+"\033[31m▀ \033[0m"+DEFAULT;
         }else if (!board.getSquare(pos).hasWall(Direction.NORTH)) {
                 gameArea[lg*5+1][col*5+2]=GREEN_BG+"  " +DEFAULT;
             }
              if (board.getSquare(pos).hasWall(Direction.SOUTH)) {
                 gameArea[lg*5+3][col*5+2]=GREEN_BG+"\033[31m▀ \033[0m"+DEFAULT;
                }else if (!board.getSquare(pos).hasWall(Direction.SOUTH)) {
             gameArea[lg*5+3][col*5+2]=GREEN_BG+"  " +DEFAULT;
               
                    }
              if (board.getSquare(pos).hasWall(Direction.EAST)) {
                gameArea[lg*5+2][col*5+3]=GREEN_BG+"\033[31m▆ \033[0m"+DEFAULT;
         }else if (!board.getSquare(pos).hasWall(Direction.EAST)) {
                        gameArea[lg*5+2][col*5+3]=GREEN_BG+"  " +DEFAULT;
             }
               if (board.getSquare(pos).hasWall(Direction.WEST)) {
             gameArea[lg*5+2][col*5+1]=GREEN_BG+"\033[31m ▆\033[0m"+DEFAULT;
         }else if (!board.getSquare(pos).hasWall(Direction.WEST)) {
                        gameArea[lg*5+2][col*5+1]=GREEN_BG+"  " +DEFAULT;
                    }
        }
            }
        }
    }
    
    /**
     * names with 2 characters the animals.
     * @param gameArea the given string tab.
     * @param animals of the game .
     * @return the representation of the current animal.
     */
    private String animalsLetters(String[][] gameArea, Animal animal){
        String currentAnimal="";
     
       if (animal instanceof Snail) {
                     currentAnimal= "S ";   
    }else if (animal instanceof Spider) {
                    currentAnimal="SP";
    }else if (animal instanceof Ladybird) {
                    currentAnimal="LB";        
       }else if (animal instanceof Butterfly) { 
                    currentAnimal="BF";        
       }else if (animal instanceof Grasshopper) {
                   currentAnimal="GH";                  
    }else if (animal instanceof Bumbelbee) {
                         currentAnimal="BB";   
     }
       return currentAnimal;
}
    
    /**
     * browse the animals and print the right depending on your levels.
     *
     * @param animals the animals on the board.
     * @param gameArea the given tab.
     */
    private void displayAnimals(String[][] gameArea, Animal... animals) {
        for (Animal animal : animals) {
            if (!animal.isOnStar()) {
                int lg = (animal.getPositionOnBoard().getRow() * 5) + 2;
                int col = (animal.getPositionOnBoard().getColumn()*5)+2; 
               gameArea[lg][col]=GREEN_BG+animalsLetters(gameArea, animal)+DEFAULT;
            }
        }
    }

    /**
     * return an error message
     *
     * @param message the given message
     */
    @Override
    public void displayError(String message) {
        System.err.println(message);
    }

    /**
     * asking the user to encode a position.
     *
     * @return the position
     */
    @Override
    public Position askPosition() {
        System.out.println("\033[33mhelp : a postion is "
  + "a cordinate such as [x,y] , row and colum start at 0\033[0m");
        int x = readInteger("which position you want to"
                + " move to?" + " enter the x");
        int y = readInteger("which position you want to"
                + " move to?" + " enter the y");
        Position position = new Position(x, y);
        System.out.println("la position donné est  "
                + "[" + x + "," + y + "]");

        return position;
    }

    /**
     * asking the user to encode a direction.
     *
     * @return the direction.
     */
    @Override
    public Direction askDirection() {
        Scanner keyboard = new Scanner(System.in);
        String save = "";
        while (!save.equals("NORTH") && !save.equals("SOUTH")
                && !save.equals("EAST") && !save.equals("WEST")) {
            System.out.println("please enter a direction : ");
            System.out.println("\033[33mhelp: South▾, North▴,"
                    + "West◂,East▸\033[0m  ");
            while (!keyboard.hasNext()) {
                System.out.println("ERROR : please enter a valid direction : ");
            }
            save = keyboard.next().toUpperCase();
        }
        switch (save) {
            case "NORTH":
                return Direction.NORTH;
            case "SOUTH":
                return Direction.SOUTH;
            case "EAST":
                return Direction.EAST;
            case "WEST":
                return Direction.WEST;
            default:
                return null;
        }
    }

    /**
     * display the number of movement left for the player depending on the
     * levels.
     *
     * @param remainingMoves represent the movement left goes down after each
     * movement.
     */
    @Override
    public void displayRemainingMoves(int remainingMoves) {
        System.out.println("moves left :" + remainingMoves);
    }

    /**
     * browse the array and print each element of it .
     *
     * @param gameArea the given board.
     */
    private void displayArray(String[][] gameArea) {
        for (String[] gameArealg : gameArea) {
            for (String gameAreacol : gameArealg) {
                if (gameAreacol != null) {
                    System.out.print(gameAreacol);
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println("");
        }
    } 
    
    /**
     * print the lines of the displayBoard.
     * 
     * @param startRow the row.
     * @param startCol the column.
     * @param gameArea  the given String tab.
     */
    private void traceMur( int startRow, int startCol,String [][]gameArea){
        for (int i = 0; i <= 4; i++) {
           gameArea[startRow][startCol+i]="▨▨";   
        }
    }
}

