/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g54627.humbug.model;

import static g54627.humbug.model.SquareType.GRASS;
import static g54627.humbug.model.SquareType.STAR;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author seshie
 */
public class BumbelbeeTest {
    
    private Board board;
    private Animal[] animals;

    /**
     * Test of move method, of class Bumbelbee .
     */
    @Test
    public void testMove_whenFall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("move_whenfall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = null;
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_general() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("move_general");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_When4Wall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(GRASS)},
            {new Square(GRASS), null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("move_When4Wall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        board.getSquare(new Position(0, 1)).setEastWall(true);
        board.getSquare(new Position(0, 1)).setWestWall(true);
        board.getSquare(new Position(0, 1)).setNorthWall(true);
        board.getSquare(new Position(0, 1)).setSouthWall(true);
        Position expResult = new Position(2, 0);
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenJumpOver1animal() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {new Square(GRASS), null, new Square(STAR), null}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0)),
            new Snail(new Position(0, 2))
        };
        System.out.println("move_WhenJumpOver1animal");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = new Position(0, 3);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_NextOnStar() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("testMove_NextOnStar");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        assertTrue(instance.isOnStar());
        assertEquals(GRASS, board.getSquareType(result));
    }
    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_NextOnStar_WhenWall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("testMove_NextOnStar_WhenWall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        board.getSquare(new Position(0,2)).setNorthWall(true);
        board.getSquare(new Position(0,2)).setSouthWall(true);
        board.getSquare(new Position(0,2)).setWestWall(true);
        board.getSquare(new Position(0,2)).setEastWall(true);
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        assertTrue(instance.isOnStar());
        assertEquals(GRASS, board.getSquareType(result));
    }
       /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenWall_nextSquare() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("testMove_WhenWall_nextSquare");
        Bumbelbee instance = (Bumbelbee) animals[0];
        board.getSquare(new Position(0,1)).setNorthWall(true);
        board.getSquare(new Position(0,1)).setSouthWall(true);
        board.getSquare(new Position(0,1)).setWestWall(true);
        board.getSquare(new Position(0,1)).setEastWall(true);
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        
    }
           /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenJumpOverEmptySquare() {
        board = new Board(new Square[][]{
            {new Square(GRASS), null, new Square(GRASS)},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("testMove_WhenJumpOverEmptySquare");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        
    }
     /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenFallWithWall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0))
        };
        System.out.println("testMove_WhenFallWithWall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        board.getSquare(new Position(0,0)).setNorthWall(true);
        board.getSquare(new Position(0,0)).setSouthWall(true);
        board.getSquare(new Position(0,0)).setWestWall(true);
        board.getSquare(new Position(0,0)).setEastWall(true);
        Position expResult = null;
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
        
    }
     /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenJumpOver2animals_withWall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null,null,null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR),null,null}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0)),
            new Bumbelbee(new Position(0, 2)),
            new Snail(new Position(0, 3)),
        };
        System.out.println("testMove_WhenWall_nextSquare_withWall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        board.getSquare(new Position(0,0)).setNorthWall(true);
        board.getSquare(new Position(0,1)).setSouthWall(true);
        board.getSquare(new Position(0,2)).setWestWall(true);
        Position expResult = new Position(0, 4);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        
    }
     /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenJumpOverAnimalAndEmptySquare_Fall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS),},
            {null, new Square(STAR), null},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 2)),
            new Bumbelbee(new Position(2, 2)),
        };
        System.out.println("testMove_WhenJumpOverAnimalAndEmptySquare_Fall");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = null;
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
        
    }
    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_JumpOverAnimal_NextOnStar() {
        board = new Board(new Square[][]{
            {new Square(GRASS),new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {null,null, new Square(GRASS), new Square(GRASS)},
            {null,null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 0)),
            new Snail(new Position(0, 2)),
        };
        System.out.println("testMove_JumpOverAnimal_NextOnStar");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = new Position(0, 3);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        assertTrue(animals[0].isOnStar());
        assertEquals(GRASS, board.getSquareType(result));
    }
    /**
     * Test of move method, of class Bumbelbee.
     */
    @Test
    public void testMove_WhenJumpOverAnimalAndEmptySquare_Fall2() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS),},
            {null, new Square(STAR), null},
            {null, null, new Square(GRASS)},
            {null, null, new Square(GRASS)}
        });
        animals = new Animal[]{
            new Bumbelbee(new Position(0, 2)),
            new Bumbelbee(new Position(2, 2)),
            new Bumbelbee(new Position(3, 2)),
        };
        System.out.println("testMove_WhenJumpOverAnimalAndEmptySquare_Fall2");
        Bumbelbee instance = (Bumbelbee) animals[0];
        Position expResult = null;
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
        
    }
    
}
