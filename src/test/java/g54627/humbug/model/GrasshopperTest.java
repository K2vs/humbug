/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g54627.humbug.model;

import static g54627.humbug.model.SquareType.GRASS;
import static g54627.humbug.model.SquareType.STAR;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author seshie
 */
public class GrasshopperTest {

    private Board board;
    private Animal[] animals;

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0))

        };
        System.out.println("move_general");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = new Position(0, 1);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenFall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0))

        };
        System.out.println("move_when_fall");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = null;
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenJumpOver_2animals() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(STAR), null, null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(1, 1)),
            new Snail(new Position(1, 2)),
            new Spider(new Position(1, 3)),};

        System.out.println("testMove_whenJumpOver_2animals");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = new Position(1, 4);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_When1Wall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR), new Square(GRASS), new Square(GRASS)},
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, null, new Square(GRASS), new Square(GRASS), null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0)),};
        System.out.println(" testMove_When1Wall");
        Grasshopper instance = (Grasshopper) animals[0];
        board.getSquare(new Position(0, 0)).setSouthWall(true);
        Position expResult = new Position(1, 0);
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);

    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_NextOnStar() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(1, 2))

        };
        System.out.println("testMove_NextOnStar");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = new Position(2, 2);
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
        assertTrue(instance.isOnStar());
        assertEquals(GRASS, board.getSquareType(result));
    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_When4Wall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(STAR), new Square(GRASS), new Square(GRASS)},
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, null, new Square(GRASS), new Square(GRASS), null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0)),};
        System.out.println(" testMove_When1Wall");
        Grasshopper instance = (Grasshopper) animals[0];
        board.getSquare(new Position(0, 0)).setSouthWall(true);
        board.getSquare(new Position(0, 0)).setNorthWall(true);
        board.getSquare(new Position(0, 0)).setWestWall(true);
        board.getSquare(new Position(0, 0)).setEastWall(true);
        //no matters the 4 wall he must passed it.
        Position expResult = new Position(1, 0);
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);

    }

    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_NextOnStar_WhenWall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(1, 2))

        };
        System.out.println("testMove_NextOnStar_WhenWall");
        Grasshopper instance = (Grasshopper) animals[0];
        board.getSquare(new Position(2, 2)).setNorthWall(true);
        Position expResult = new Position(2, 2);
        Position result = instance.move(board, Direction.SOUTH, animals);
        assertEquals(expResult, result);
        assertTrue(instance.isOnStar());
        assertEquals(GRASS, board.getSquareType(result));
    }
     /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenWallAndFall() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS)},
            {null, null, new Square(STAR)}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0))

        };
        System.out.println("testMove_whenWallAndFall");
        Grasshopper instance = (Grasshopper) animals[0];
        board.getSquare(new Position(0,0)).setWestWall(true);
        Position expResult = null;
        Position result = instance.move(board, Direction.WEST, animals);
        assertEquals(expResult, result);
    }
     /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenJumpOver_1animals() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(STAR), null, null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0)),
            new Snail(new Position(0, 1)),
            };

        System.out.println("testMove_whenJumpOver_1animals");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }
    /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenJumpOver_2animals_ArriveOnStar() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(STAR)},
            {null, new Square(GRASS), new Square(GRASS), null, null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(1, 1)),
            new Snail(new Position(1, 2)),
            new Spider(new Position(1, 3)),};

        System.out.println("testMove_whenJumpOver_2animals_ArriveOnStar");
        Grasshopper instance = (Grasshopper) animals[0];
        Position expResult = new Position(1, 4);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
        assertTrue(instance.isOnStar());
        assertEquals(GRASS,board.getSquareType(result));
    }
      /**
     * Test of move method, of class grasshopper.
     */
    @Test
    public void testMove_whenJumpOver_1animals_ArrivedOnWallSquare() {
        board = new Board(new Square[][]{
            {new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS), null},
            {null, new Square(GRASS), new Square(GRASS), new Square(GRASS), new Square(GRASS)},
            {null, new Square(GRASS), new Square(STAR), null, null}
        });
        animals = new Animal[]{
            new Grasshopper(new Position(0, 0)),
            new Snail(new Position(0, 1)),
            };

        System.out.println("testMove_whenJumpOver_1animals_ArrivedOnWallSquare");
        Grasshopper instance = (Grasshopper) animals[0];
        board.getSquare(new Position(0,2)).setSouthWall(true);
        board.getSquare(new Position(0,2)).setEastWall(true);
        Position expResult = new Position(0, 2);
        Position result = instance.move(board, Direction.EAST, animals);
        assertEquals(expResult, result);
    }
}
